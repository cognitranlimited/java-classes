package com.cognitran.classes.topic3.animals;

public class Tiger extends Cat {

    public Tiger() {
        super();
    }

    public Tiger(String name) {
        super(name, "tygrysi");
    }

    public Tiger(String name, String color) {
        super(name);
    }

    protected void say(String what) {
        super.say(what + " !!!wrr!!");
    }

    public void kill() {
        say("zabijam");
    }


    public void playHard() {
        kill();
    }
}
