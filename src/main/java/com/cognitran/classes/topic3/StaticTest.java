package com.cognitran.classes.topic3;

import static com.cognitran.classes.topic3.animals.Cat.getCount;
import static java.lang.Math.PI;
import static java.lang.Math.sin;

public class StaticTest {
    public static void main(String[] args) {

        System.out.println( sin(2 * PI) );

        System.out.println(getCount());

    }
}
