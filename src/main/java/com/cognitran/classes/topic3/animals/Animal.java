package com.cognitran.classes.topic3.animals;

public abstract class Animal {

    private static int id = 0;
    protected String name;

    public Animal() {
        this("NO_NAME_" + id);
    }

    public Animal(String name) {
        this.name = name;
        id++;
    }

    public String getName() {
        return name;
    }

    protected void say(String what) {
        System.out.println(getName() + ": -" + what);
    }

    public abstract void play();

}
