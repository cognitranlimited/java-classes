package com.cognitran.classes.collections;

import com.cognitran.classes.topic3.animals.BadDog;
import com.cognitran.classes.topic3.animals.Dog;

public class CollectionExample {
    public static void main(String[] args) {


        Dog[] dogs  = new Dog[10];

        for (int i = 0; i < dogs.length; i++) {
            dogs[i] = new Dog("Burek_" + i);
        }



//        for (Dog dog : dogs) {
//            System.out.println("Pies: " + dog);
//            dog = new Dog("Burek");
//        }

        for (Dog dog : dogs) {
            System.out.println("Pies2: " + dog);
        }

//        int[] xs  = new int[10];
//        for (int x : xs) {
//            System.out.println("x: " + x);
//        }

    }
}
