class: center, middle, inverse
#  Cognitran uczy Javy

---
#  Agenda
- O firmie, o mnie, o pracy programisty
- Java - wersje, popularność, zastosowanie
- Pierwszy program - instrukcje warunkowe, typy, pętle

---
# O nas
- Ja
- Cognitran

---
<img src="images/top_programming_languages.png"/>
---
## Czy sama znajomośc Javy wystarczy by być dobrym programistą?
---

## Niestety nie:

- znajomość języków obcych (angielski, niemiecki)
- znajomość baz danych (SQL)
- znajomość poleceń linuksa
- znajomość języków skryptowych
- znajomość innych języków programowania

---

## Java - wprowadzenie

<img src="images/Java_programming_language_logo.svg" style="float: right" width="25%" />

- Język Java

- Platforma Java
	- Maszyna wirtualna - JVM
	- Java API

- Edycje Javy
	- Java Standard Edition (Java SE)
	- Java Enterprise Edition (Java EE)
	- Java Micro Edition (Java ME)
	- Java Card

- Wersje Javy (5-10)
---
##Zalety i wady Javy
- Zalety
		- Darmowość - sama Java, IDE oraz cała masa bibiotek jest dostępna zupełnie za darmo.
		- Przenośność - możesz uruchomić program na każdym systemie i sprzęcie,
		  na którym zainstalowano wirtualną maszynę Javy.
		- Duże wsparcie ze strony twórców środowisk programowania - dzięki środowiskom IDE,
		  takim jak Eclipse czy IntelliJ, programy możesz tworzyć jeszcze szybciej i jeszcze
		  wydajniej używając profesjonalnych narzędzi, które ułatwią tworzenie dużych aplikacji
		  oraz panowanie nad tworzeniem oraz utrzymywaniem kodu.
		- duża liczba rozszerzeń, bibliotek, frameworków
		- szybkość tworzenia aplikacji - pisze się w języku naturalnym (angielskim).
		- duża ilość publikacji - ksiązki, szkolenia, tutoriale, fora.
		- ogromna społecznośc programistów
		- Garbage collector - proces działający w tle automatycznie usuwa
		  nieużywane obiekty z pamięci.
- Wady
		- Przeważnie programy uruchamiane pod maszyną wirtualną działają wolniej
		  niż programy napisane w języku niższego poziomu np. w C/C++
		  (szczególnie czasochłonne jest uruchamianie aplikacji).
		- Użytkownik potrzebuje zainstalowanej maszyny wirtualnej javy (JVM), aby móc uruchomić program.
		- niepełna kompatybilność wstecz

---

## Java
#### Kompilacja i JVM

- Kompilacja do kodu pośredniego (*bytecode*)
- Uruchomienie przez JVM
- Niezależność od platformy

<br/>
<img src="images/java_jvm.svg" width="100%" />

---
## Java
#### Potrzebne narzędzia

- Java
	- Java Development Kit - JDK
	- Java Runtime Environment - JRE
	
	http://www.oracle.com/technetwork/java/javase/downloads/

<br/>

- Środowisko IDE
	- Eclipse, http://www.eclipse.org
	- Netbeans, http://www.netbeans.org
	- IntelliJ IDEA, https://www.jetbrains.com/idea/
---

## Java
#### Typy danych i zmienne

- Typowanie statyczne, ścisła kontrola typów

- Typy proste (*primitives*)
	- `int`, `long`, `short`, `byte`
	- `float`, `double`
	- `boolean`
	- `char`

- Typy obiektowe
	- `String`
	- `Object`, `ArrayList`, `Student`, `HelloWorld`, ...
	- `Integer`, `Double`, `Boolean`, `Float`, ...
	
- Typ wyliczeniowy - `enum`

- Wszystkie obiekty dziedziczą po klasie java.lang.Object

---

# Pierwszy program
``` java
package com.cognitran.classes.lesson1;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.print("Hello world");
    }
}
```

- Pakiet (folder) `com.cognitran.classes.lesson1`
- Klasa (plik) `HelloWorld`
- Metoda `main(String[] args)`

Konwencje nazewnicze - CamelCase

---
# Komputer liczy

``` java
    public static void main(String[] args) {
        int a = 2;
        int b = 3;

        System.out.println("a = " + a);
        System.out.println("b = " + b);

        int c = a + b;

        System.out.println("a + b = " + c);

        c = a * b;
        System.out.println("a * b = " + c);
    }
```

---
# Instrukcje warunkowe 1/3
instrukcja `if`
```java
    public static void main(String[] args) {
        double x = Math.random() * 5;

        System.out.println("x = " + x);

        if (x > 3) {
            System.out.println("x > 3");
        }

    }
```

---
# Instrukcje warunkowe 2/3
instrukcja `if / else`
```java
    public static void main(String[] args) {
        double x = Math.random() * 5;

        System.out.println("x = " + x);

        if (x > 3) {
            System.out.println("x > 3");
        }

        if (x > 4) {
            System.out.println("x > 4");
        } else {
            System.out.println("x nie > 4");
        }
    }
```

---
# Instrukcje warunkowe 3/3
instrukcja `if / if else / if`
```java
    public static void main(String[] args) {
        double x = Math.random() * 5;

        System.out.println("x = " + x);

        if (x > 3) {
            System.out.println("x > 3");
        } else if (x > 2) {
            System.out.println("x > 2");
        } else {
            System.out.println("x nie > 2");
        }
    }
```

---
# Return
``` java
    public static void main(String[] args) {
        double x = Math.random() * 5;

        System.out.println("x = " + x);

        System.out.println("Test na 3");
        if (x > 3) {
            System.out.println("x > 3");
            return;
        }

        System.out.println("Test na 2");
        if (x > 2) {
            System.out.println("x > 2");
            return;
        }

        System.out.println("Test na 1");
        if (x > 1) {
            System.out.println("x > 1");
            return;
        }
    }
```

---
# String 1/2
* inicjalizacja
``` java
String s = "Leszek"
```
* sprawdzanie czy string zaczyna się od tekstu
```java
s.startsWith("Lesz"); //true
s.startsWith("L"); //true
s.startsWith("l"); //false
```
* sprawdzanie czy string zawiera tekst
``` java
s.contains("sz"); //true
s.contains("SZ"); //false
s.contains("d"); //false
```

---
#String 2/2
``` java
String s = "Leszek"
```
* zmiana tekstu na małe litely
``` java
"LeSzEk".toLowerCase(); //"leszek"
```
* pobieranie części tekstu
``` java
s.substring(2); //"szek"
s.substring(2, 4); //"sz"
```
* porównywanie tekstu
``` java
s.equals("Leszek"); //true
s.equals("leszek"); //false
s.equalsIgnoreCase("leszek"); //true
```

---
# Operatory logiczne
* `&&` - and `||` - or `!` - not
* `&` `|`

``` java
    public static void main(String[] args) {
        String n = "aBr";

        if (n.contains("a") && n.contains("b")) {
            System.out.println("Zawiera a i b");
        }
        if (n.contains("a") || n.contains("A")) {
            System.out.println("Zawiera a lub A");
        }
        if (n.contains("a") || n.contains("A")
                && n.contains("b") || n.contains("B")) {
            System.out.println("Zawiera a lub A i b lub B");
        }
        if ((n.contains("a") || n.contains("A"))
                && (n.contains("b") || n.contains("B"))) {
            System.out.println("Zawiera a lub A i b lub B (2 wersja)");
        }
        if ((n.contains("a") || n.contains("A")) && (n.contains("b")
                || n.contains("B")) && !n.contains("r")) {
            System.out.println("Zawiera a lub A i b lub B i nieprawda, że r");
        }
    }
```

---
# Zadanie 1
Napisz kod poradnika dietetycznego, analizującego następujące dane:
- wiek
- wzrost
- wagę

Na ich podstawie poradnik wyświetla na ekranie odpowiednie wskazówki:
1. nie udzielamy porad dla absurdalnych wartości
2. nie udzielamy porad ludziom starszym od 100 lat
3. porady dla nieletnich opatrzone specjalną klauzulą
4. ostre ostrzeżenie dla dziesięciolatków > 100kg
5. ostrzeżenie anorektyczne - dla osób > 10 lat, lżejszych od 40kg i większych od 1m
6. dla cięższych niż 100kg uwaga w zależności od wzrostu (dla wyższych niż 2m waga jest uzasadniona)

---
# Metody 1/2
``` java
public class MethodExample {

    static void calculate(int a, int b) {
        int c = a + b;
        System.out.println("argument a: " + a);
        System.out.println("argument b: " + b);
        System.out.println("obliczono: " + c);
    }

    public static void main(String[] args) {
        int x = 3;
        int y = 4;

        calculate(x, y);
        calculate(12, 35);
    }
}
```

---
# Metody 2/2
  Metody zwracają wynik.
  Wynik działa metody można zignorować.
``` java
public class Method2Example {
    static int calculate(int a, int b) {
        int c = a + b;
        System.out.println("\nargument a: " + a);
        System.out.println("argument b: " + b);
        System.out.println("obliczono: " + c);
        return c;
    }

    public static void main(String[] args) {
        int x = 3;
        int y = 4;
        int z = calculate(x, y);
        System.out.println("Wynik metody: " + z);
        System.out.println("Wynik metody: " + calculate(12, 35));
        calculate(1, 2);
        calculate(1, calculate(2, 3));
        calculate(calculate(1, 2) + calculate(3, 4), calculate(2, 3));
    }
}

```

---
# Kalkulator
``` java

public class CalculatorExample {
    public static void main(String[] args) {
        System.out.println(add(1, 2));
        System.out.println(multiple(2, 2));
        System.out.println(substract(10, 2));
        System.out.println(divide(4, 2));
        System.out.println(calculate(4, 2, "+"));
        System.out.println(calculate(4, 2, "plus"));
        calculate(4, 2, "print +");
        calculate(4, 2, "print -");
        System.out.println(add(1, 2, 3));
    }
}
```

```
3
4
8
2
6
6
4 + 2 = 6
4 - 2 = 2
6
```



---
# Tworzymy klasę
```java
public class Cat {

    String name;

    void play() {
        System.out.println("Jestem kotek " + name + " i się bawię.");
    }
}
```

---
# Tworzymy obiekt
referencje, a obiekty, ćwiczenia
```java
        Cat c1 = new Cat();
        c1.name = "Bonifacy";
        c1.play();

        Cat c2 = new Cat();
        c2.name = "Filemon";
        c2.play();

        Cat c3 = c1;
        c3.play();

        System.out.println("##############");
        c1.name = "Boniek";
        c1.play();
        c3.play();
```
garbage collector
referencja, a obiekt w pamięci!

---
# Pakiety 1/2
#### import klasy z innego pakietu
#### modyfikator public


---
# Pakiety 2/2
#### konflikt nazw

```java
package com.cognitran.classes.topic3;

import com.cognitran.classes.topic3.animals.Cat2;

public class PackagesImporting {
    public static void main(String[] args) {
        Cat2 cat = new Cat2();
        cat.name = "Filemon";
        cat.play();

        com.cognitran.classes.topic3.caterpilar.Cat2 caterpilar = new com.cognitran.classes.topic3.caterpilar.Cat2();
        caterpilar.dig();
    }
}
```

---
# Enkapsulacja 1/2
- konstruktor
- modyfikator private
- getters / setters
- this.

```java
public class Cat3 {
    private String name;
    private String color;

    public Cat3(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public void setColor(String color) {
        this.color = color;
    }

    void play() {
        System.out.println("Jestem kotkiem " + name + " koloru" + color + "  i się bawię.");
    }
}
```

---
# Enkapsulacja 2/2
- prywatne metody
- reużywanie metod

```java
public class Cat4 {
    private String name;
    private String color;

    private void say(String what) {
        System.out.println(name + "(kot): " + what);
    }

    public Cat4(String name) {
        this.name = name;
    }

    public String getName() { return name; }

    public void setColor(String color) { this.color = color; }

    public void play() {
        System.out.println("bawię się w kolorze " + color);
    }
}
```

---
# final 1/2
#### final jako zmienna (stała?)
```java
    public static void main(String[] args) {
        System.out.println("Obiekty");

        final Cat4 c = new Cat4("Bonifacy");
        c.setColor("niebieski");

        c.play();

//        c = new Cat4("Filemon");
        c.setColor("zielony");
        c.play();
    }
```

---
# final 2/2
#### final jako pole i jako argument
```java
public class Cat5 {
    private final String name;
    private String color;

    private void say(final String what) {
        System.out.println(name + "(kot): " + what);
    }

    public Cat5(final String name) { this.name = name; }

    public Cat5(final String name, final String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() { return name; }

    public void setColor(final String color) { this.color = color; }

    public void play() {
        say("bawię się w kolorze " + color);
    }
}
```

---
# static 1/2
- pole static
- pole final static

```java
public class Cat6 {
    public static final int LEGS_COUNT = 4;
    public static String DEFAULT_COLOR = "biały";

    private String name;
    private String color;

    public Cat6(String name) {
        this.name = name;
        color = DEFAULT_COLOR;
    }
}
```

---
# static 2/2
#### używanie pól static z zewnątrz

```java
public class CatsAndStatics {
    public static void main(String[] args) {
        Cat6 c1 = new Cat6("Bonifacy");
        c1.play();

        System.out.println(c1.LEGS_COUNT);
        System.out.println(Cat6.LEGS_COUNT);

        System.out.println(c1.defaultColor);
        System.out.println(Cat6.defaultColor);

        c1.defaultColor = "czarny";

        Cat6 c2 = new Cat6("Bonifacy");
        c2.play();
    }
}
```

---
# zadanie
- Dopisz statyczną(?) metodę countCatsLegs obliczającą liczbę nóg na podstawie liczby kotów
- Utwórz unikalny identyfikator dla kota (zwiększający się, niemożliwy do zmiany, niedostępny z zewnątrz, wyświetlany w 'say')
- Oblicz liczbę kotów, pozwól ją wyświetlić.

```java
Cat7 c1 = new Cat7("Bonifacy", "zielony");
c1.play();

new Cat7("Bezdomny");

Cat7 c2 = new Cat7("Filemon");
c2.play();

System.out.println("Mamy: " + Cat7.getCatsCount() + " kotów.");
```


---
#rozwiązanie
```java
public class Cat7 {
    public static final int LEGS_COUNT = 4;
    public static String defaultColor = "biały";
    private static int catsCount = 0;

    private final int id;
    private final String name;
    private String color;

    public Cat7(String name) {
        this.name = name;
        color = defaultColor;
        id = catsCount++;
    }

    public Cat7(String name, String color) {
        this.name = name;
        this.color = color;
        id = catsCount++;
    }

    public static int getCatsCount() { return catsCount; }

    private void say(final String what) {
        System.out.println(name + "(kot:" + id + "): " + what);
    }

    public void play() { say("bawię się w kolorze " + color); }
}
```

---
# przeciążanie metod / overloading
```java
public void play() {
    say("bawię się w kolorze " + color);
}

public void play(String how) {
    say("bawię się w kolorze " + color + " w " + how);
}
```

---
# dziedziczenie


