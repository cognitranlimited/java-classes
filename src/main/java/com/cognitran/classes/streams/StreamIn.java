package com.cognitran.classes.streams;

import org.junit.Test;

import java.io.*;

public class StreamIn {

    @Test
    public void readIntByInt() throws Exception {

        InputStream inStream = new FileInputStream("../../file.dat");
        int b = inStream.read();

        while (b > 0) {

            System.out.println("B:" + String.format("%x %d", b, b));

            b = inStream.read();
        }

    }


    @Test
    public void readUsingArrays() throws Exception {
        InputStream inStream = new FileInputStream("../../file.dat");

        int b = inStream.read();

        byte[] bytes = new byte[5];

        int read;
        while( (read = inStream.read(bytes)) > 0 ) {
            System.out.println(read);

            for (int i = 0; i < read; i++) {
                byte aByte = bytes[i];
                System.out.println("B:" + String.format("%x %d", aByte, aByte));

            }
        }


    }
}
