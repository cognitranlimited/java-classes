package com.cognitran.classes.topic3.animals;

public class Eagle extends Bird implements AdvancedFly {

    public Eagle(String name) {
        super(name);
    }

    @Override
    public void fly(AdvancedFly another) {
        say("Latamy razem {");
        fly();
        another.fly();
        say("}");
//        say( getName() + " flying with " + another.getName());
    }

    @Override
    public void play() {
        fly();
    }

    @Override
    public void fly() {
        say("Eagle can fly");
    }
}
