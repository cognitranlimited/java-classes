package com.cognitran.classes.topic3.animals;

public class BadDog extends Dog {

    private final Dog friend;

    public BadDog(String name) {
        super(name);
        friend = null;
    }

    public BadDog(String name, Dog friend) {
        super(name);
        this.friend = friend;
    }

    public void attack(Animal enemy) {
        if (this == enemy) {
            return;
        }

        if (enemy == friend) {
            return;
        }

        say("gryzę: " + enemy.getName());

        if (friend != null && friend instanceof BadDog) {
            ((BadDog) friend).attack(enemy);
        }

        if (enemy instanceof BadDog) {
            enemy.say("Odgryzam się: " + getName());
//            ((BadDog) enemy).attack(this);
        }

    }
}
