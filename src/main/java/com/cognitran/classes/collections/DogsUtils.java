package com.cognitran.classes.collections;

import com.cognitran.classes.topic3.animals.Dog;

public class DogsUtils {

    Dog[] createDogs(int howMany) {
        Dog[] dogs = new Dog[howMany];

        for (int i = 0; i < dogs.length; i++) {
            dogs[i] = new Dog("Burek_" + i, (int)(Math.random() * 10));
        }

        return dogs;
    }

    Dog[] filterDogs(Dog[] allDogs) {

        int oldCount = 0;
        for (Dog dog : allDogs) {
            if (dog.age > 5) {
                oldCount++;
            }
        }

        int i = 0;
        Dog[] oldDogs = new Dog[oldCount];

        for (Dog dog : allDogs) {
            if (dog.age > 5) {
                oldDogs[i++]  = dog;
            }
        }

        return  oldDogs;
    }

}
