package com.cognitran.classes.topic3;

import com.cognitran.classes.topic3.animals.Animal;
import com.cognitran.classes.topic3.animals.Cat;
import com.cognitran.classes.topic3.animals.Tiger;

public class Animals8 {
    public static void main(String[] args) {

//        new Animal("lala");
//
//        Cat cat = new Cat();
//        Cat tigerAsCat = new Tiger("Bonzo");
//        Tiger tiger = new Tiger();
//        Animal catAsAnimal = new Cat("Bonifacy");
//
//        cat.play();
//        tigerAsCat.play();
//        tiger.play();
//        catAsAnimal.play();
//

        AnimalFactory animalFactory = new AnimalFactory();
        Animal[] animals = animalFactory.createAnimals();

//        Animal[] animals = new Animal[] {new Cat("Cat1"), new Tiger("Tiger2")};

        for (Animal animal : animals) {
            animal.play();
        }



//        System.out.println("All cats: " + Cat8.getCount());
    }
}
