package com.cognitran.classes.topic3.animals;

public class Duck extends Bird implements CanSwim {

    public void swim() {
        say("Swiming");
    }

    public void fly() {
        say("Flying");
    }

    public void play() {
        say("Duck can have real fun");
        swim();
        fly();
    }
}
