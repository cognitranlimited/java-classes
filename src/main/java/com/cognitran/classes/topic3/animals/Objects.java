package com.cognitran.classes.topic3.animals;

public class Objects {

    public static void main(String[] args) {
        System.out.println("Obiekty");

        Cat1 c1 = new Cat1();
        c1.name = "Bonifacy";
        c1.play();

        Cat1 c2 = new Cat1();
        c2.name = "Filemon";
        c2.play();

        Cat1 c3 = c1;
        c3.play();

        System.out.println("##############");
        c1.name = "Boniek";
        c1.play();
        c3.play();
    }
}
