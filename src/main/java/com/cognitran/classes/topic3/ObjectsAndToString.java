package com.cognitran.classes.topic3;

import com.cognitran.classes.topic3.animals.Dog;

public class ObjectsAndToString {
    public static void main(String[] args) {

        Dog d1 = new Dog("Kajtek");
        d1.play();

        String znak = "A";
        String x1 = "";

        for (int i = 0; i < 10; i++) {
            x1 += znak;

        }

        System.out.println(x1);


        Integer x = new Integer(12);
        Dog d2 = new Dog("Bonzo");

        Object o1 = getKajtek();
//        System.out.println(o1.getClass());

        System.out.println(o1.toString());
        System.out.println(o1);

        System.out.println(Integer.toHexString(o1.hashCode()));


        System.out.println(new Integer(11));

//        print(o1);
    }

    private static Object getKajtek() {
        return new Dog("Kajtek");
    }


    private static void print(String x) {
        System.out.printf("[[[" + x + "]]]");
    }
}
