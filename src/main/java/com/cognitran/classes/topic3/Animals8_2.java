package com.cognitran.classes.topic3;

import com.cognitran.classes.topic3.animals.*;

public class Animals8_2 {
    public static void main(String[] args) {

        AnimalFactory animalFactory = new AnimalFactory();
//        Animal[] animals = animalFactory.createAnimals();
//
//
//        for (Animal animal : animals) {
//            animal.play();
//        }
//
//        Tiger t1 = (Tiger) animals[1];
//        t1.kill();

//        ((Tiger)animals[0]).kill();


//        Animal a = null;
//
//        Dog d = null;
//
//        Cat c = null;
//
//        a = d;
//
//        d = (Dog) a;
//
//        a = c;
//
//        c = (Cat) a;
//
//        d = (Dog)(Animal)c;


        System.out.println("**** random ****");
        Animal[] randomAnimals = animalFactory.createAnimals(5);


        int i = 0;
        for (Animal animal : randomAnimals) {
            System.out.println("** " + i++ );
            animal.play();

            if (animal instanceof Tiger) {
                Tiger ti = (Tiger) animal;
                ti.kill();
            }
        }

//        System.out.println("All cats: " + Cat.getCount());


    }
}
