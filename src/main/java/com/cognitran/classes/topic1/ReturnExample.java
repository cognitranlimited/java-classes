package com.cognitran.classes.topic1;

public class ReturnExample {
    public static void main(String[] args) {
        double x = Math.random() * 5;

        System.out.println("x = " + x);

        System.out.println("Test na 3");
        if (x > 3) {
            System.out.println("x > 3");
            return;
        }

        System.out.println("Test na 2");
        if (x > 2) {
            System.out.println("x > 2");
            return;
        }

        System.out.println("Test na 1");
        if (x > 1) {
            System.out.println("x > 1");
            return;
        }
    }
}
