package com.cognitran.classes.topic3;

import com.cognitran.classes.topic3.animals.*;

public class AnimalsInterfaces {
    public static void main(String[] args) {

        Animal duck = new Duck();
        duck.play();

        CanFly bird = new Eagle("Szary");
        bird.fly();

        Duck duck1 = new Duck();
        duck1.fly();
        duck1.swim();
        duck1.play();

        CanSwim canSwim = duck1;
        canSwim.swim();

        CanFly canFly = duck1;
        canFly.fly();

//        CanAll canAll = duck1;

//        CanSwim ss = new CanSwim();


    }
}
