package com.cognitran.classes.collections;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ListsProblems3 {

    static List<StringBuilder> deepCopy(List<StringBuilder> originals) {
        List<StringBuilder> realCopy = new ArrayList<>(originals.size());

        for (StringBuilder s : originals) {
            realCopy.add(new StringBuilder(s));
//            realCopy.add(s);
        }

        return realCopy;
    }

    public static void main(String[] args) {

        StringBuilder[] array = {new StringBuilder("A"), new StringBuilder("B"), new StringBuilder("C")};

        List<StringBuilder> list = Arrays.asList(array);

        List<StringBuilder> copy = new ArrayList<StringBuilder>(list);

        List<StringBuilder> realCopy = deepCopy(list);

        array[0].append("!");
        list.get(1).append("#");
        copy.get(1).append("%");
        realCopy.get(2).append("$");

        for (StringBuilder stringBuilder : array) {
            System.out.println(stringBuilder);
        }
        for (StringBuilder stringBuilder : list) {
            System.out.println(stringBuilder);
        }
        for (StringBuilder stringBuilder : copy) {
            System.out.println(stringBuilder);
        }
        for (StringBuilder stringBuilder : realCopy) {
            System.out.println(stringBuilder);
        }

    }
}
