package com.cognitran.classes.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListsProblems2 {

    static void printAndChange(final String s) {
        System.out.println(s);
//        s += "!";
        String s2 = new String(s + "!");
        System.out.println(s2);
    }

    static void printAndChange(final StringBuilder s) {
        System.out.println(s);
        s.append("!");
        System.out.println(s);
        s.append("%").append("#").append("$");
        System.out.println(s);
    }



    public static void main(String[] args) {

        String x = "Zenek";
        printAndChange(x);
        System.out.println(x);

        StringBuilder x2 = new StringBuilder("Heniek");
        printAndChange(x2);
        System.out.println(x2);


//        StringBuilder sb = new StringBuilder();
//        sb.append("Ala");
//        sb.append(" ma ");
//        sb.append("kota");
//
//        System.out.println(sb);

    }
}
