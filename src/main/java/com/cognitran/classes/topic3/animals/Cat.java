package com.cognitran.classes.topic3.animals;

public class Cat extends Animal {
    final static public String DEFAULT_COLOR = "biały";
    private static int count = 0;

    public String color;

    public Cat() {
        this("NO_NAME_CAT_" + count);
    }

    public Cat(String name) {
        this(name, DEFAULT_COLOR);
    }

    public Cat(final String name, final String color) {
        super(name);
        this.color = color;
        count++;
    }

    public void play() {
        say("Bawię się włóczką");
    }

    public void playHard() {
        say("gonię za czerwoną kropką");
    }

    public static int getCount() {
        return count;
    }

}
