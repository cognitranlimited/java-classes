package com.cognitran.classes.topic3.animals;

public interface CanAll extends CanFly, CanSwim {

    default void canDoMagic(){
        swim();
        fly();
    }

}
