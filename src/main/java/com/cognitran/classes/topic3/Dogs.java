package com.cognitran.classes.topic3;

import com.cognitran.classes.topic3.animals.BadDog;
import com.cognitran.classes.topic3.animals.Dog;
import com.cognitran.classes.topic3.animals.Tiger;

public class Dogs {

    public static void main(String[] args) {

//     1. Pies może mieć przyjaciela (jeżeli ma, to jest to przyjaciel na całe życie, nigdy się nie zmieni
//     2. Zły pies umie atakować dowolne zwierze
//     3. Zły pies nigdy nie zaatakuje siebie
//     4. Zły pies nigdy nie zaatakuje swojego przyjaciela (jeżeli go ma)
//     5. Jeżeli zły pies ma przyjacia, który jest też złym psem, podczas atakowania atakują obydwoje
//     6. Jeżeli atakowane zwierzę jest złym psem - odgryzie się atakującemu

        Dog kajtek = new Dog("Kajtek");

        Dog pluto = new Dog("Pluto");
        BadDog buldog = new BadDog("Buldog", pluto);

        BadDog doberman = new BadDog("Doberman", new BadDog("Doberman2"));
//        BadDog doberman = new BadDog("Doberman", new BadDog("Doberman2", new BadDog("Doberman3")));


//        buldog.attack(null);

//        System.out.println((null instanceof Object));

        System.out.println("\nBuldog atakuje Kajtka");
        buldog.attack(kajtek);

        System.out.println("\nBuldog atakuje Tygrysa");
        buldog.attack(new Tiger("Bonzo"));

        System.out.println("\nCzy Buldog zaatakuje siebie samego?");
        buldog.attack(buldog);

        System.out.println("\nCzy Buldog zaatakuje pluto?");
        buldog.attack(pluto);

        System.out.println("\nBuldog atakuje dobermana");
        buldog.attack(doberman);

        System.out.println("\nDoberman atakuje Kajtka");
        doberman.attack(kajtek);

    }

}
