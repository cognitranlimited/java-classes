package com.cognitran.classes.topic3;

import com.cognitran.classes.topic3.animals.*;

public class AnimalFactory {

    static int id = 0;

    public Animal[] createAnimals() {
        return new Animal[] {new Cat("Cat1"), new Tiger("Tiger2")};
    }

    public Animal[] createAnimals(int howMany) {
        Animal[] animals = new Animal[howMany];

        for (int i = 0; i < animals.length; i++) {
            animals[i] = createAnimal();
        }

        return animals;
    }

    private Animal createAnimal() {
        Animal animal;
        int animalType = (int) (Math.random() * 5);

        switch (animalType) {
            case 0:
                animal = new Cat();
                break;
            case 1:
                animal = new Tiger("Bengal");
                break;
            case 2:
                animal = new Dog("Bingo");
                break;
            case 3:
                animal = new Duck();
                break;
            case 4:
                animal = new MagicTiger("Alladyn");
                break;

            default:
                throw new IllegalStateException("Nie ma zwierzęcia tego typu: " + animalType);
        }
        return animal;
    }

    public CanFly[] createFlyingAnimals(int howMany) {
        CanFly[] animals = new CanFly[howMany];

        for (int i = 0; i < animals.length; i++) {
            animals[i] = createFlyingAnimal();
        }

        return animals;
    }

    private CanFly createFlyingAnimal() {
        CanFly animal;
        int animalType = (int) (Math.random() * 4);

        switch (animalType) {
            case 0:
                animal = new Duck();
                break;
            case 1:
                animal = new Eagle("igel_" + id++);
                break;
            case 2:
                animal = new Duck();
                break;
            case 3:
                animal = new MagicTiger("Alladyn");
                break;

            default:
                animal = null;
                break;
        }
        return animal;
    }


}
