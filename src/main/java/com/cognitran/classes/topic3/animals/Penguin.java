package com.cognitran.classes.topic3.animals;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.Serializable;

public class Penguin extends Bird implements CanSwim, Serializable {

    @Override
    public void play() {
        swim();
    }

    @Override
    public void swim() {
        say("płynę");
    }

    @Override
    public void fly() {
        throw new NotImplementedException();
    }
}
