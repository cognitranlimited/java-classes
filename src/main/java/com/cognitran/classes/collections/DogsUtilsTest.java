package com.cognitran.classes.collections;

import com.cognitran.classes.topic3.animals.Animal;
import com.cognitran.classes.topic3.animals.Cat;
import com.cognitran.classes.topic3.animals.Dog;

import java.util.*;

public class DogsUtilsTest {

    public static Collection<Dog> createDogs(int howMany) {
        Set<Dog> dogs = new TreeSet<>();

        for (int i = 0; i < howMany; i++) {
            dogs.add(new Dog("Burek_" + i));
        }

        return dogs;
    }


    public static void main(String[] args) {
        Collection<Dog> dogs = createDogs(10);

        for (Dog dog : dogs) {
            System.out.println(dog);
        }
//
//        Object[] array = dogs.toArray();
//        Dog d1 = (Dog) array[2];
//
//        System.out.println("##################");
//        System.out.println(d1);
//
//        if(dogs.contains(d1)){
//            System.out.println("d1 jest na liście");
//        }
//
//        System.out.println(dogs.remove(d1));
//
//        System.out.println("################## 2");
//        for (Dog dog : dogs) {
//            System.out.println(dog);
//        }

    }

    private static void lists() {
        new ArrayList<Dog>();

        List<Dog> dogs = new ArrayList<>();

        dogs.add(new Dog("Burek1"));
        dogs.add(new Dog("Burek2"));
        dogs.add(new Dog("Burek3"));

        System.out.println(dogs.get(2));
        dogs.set(1, new Dog("b"));

        for (Dog dog : dogs) {
            System.out.println(dog);
            dog.play();
        }
    }

    private static void dogsArrays() {
        DogsUtils dogsUtils = new DogsUtils();

        Dog[] allDogs = dogsUtils.createDogs(10);

        System.out.println("********* ALL ***************");
        for (Dog dog : allDogs) {
            System.out.println(dog);
        }

        Dog[] oldDogs = dogsUtils.filterDogs(allDogs);

        System.out.println("********* OLD ***************");
        for (Dog dog : oldDogs) {
            System.out.println(dog);
        }
    }
}
