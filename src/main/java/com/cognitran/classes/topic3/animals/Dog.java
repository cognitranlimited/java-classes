package com.cognitran.classes.topic3.animals;

public class Dog extends Animal implements Comparable {

    public int age;

    public Dog(String name) {
        super(name);
        age = (int) (Math.random() * 5);
    }

    public Dog(String name, int age) {
        super(name);
        this.age = age;
    }

    public void play() {
        say("Wow wow");
    }


    @Override
    public String toString() {
        return "Dog {" +
                "age=" + age +
                ", name='" + name + '\'' +
                "} ";
    }

    @Override
    public int compareTo(Object o) {
        Dog anotherDog = (Dog) o;

//        return age - anotherDog.age;


        if (age > anotherDog.age) {
            return -1;
        } else if (age < anotherDog.age) {
            return 1;
        } else {
            return 0;
        }
    }
}
