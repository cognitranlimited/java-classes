package com.cognitran.classes.topic1.task1.solution;

public class Solution2 {

    static void giveAnAdvice(int age, int weight, int height) {
        System.out.println("\nAnaliza dietetyczna dla człowieka w wieku: " + age + " o wzroście: " + height + " i wadze: " + weight);

        int adviceCount = 0;

        //1
        if (weight < 10 || weight > 300 || height < 50 || height > 300 || age < 1 || age > 200) {
            System.out.printf("Przepraszam, nie umiem nic doradzić");
            return;
        }

        //2
        if (age > 100) {
            System.out.println("Jeżeli ktoś przeżył tyle lat, nie potrzebuje porad.");
            return;
        }

        //3
        if (age < 18) {
            System.out.println("Porady dla nieletnich należy zawsze skonsultować z lekarzem.");
        }

        //4
        if (weight > 100 && age < 10) {
            System.out.println("To zdecydowana nadwaga! - nie powinno się tyle ważyć w tak młodym wieku");
            adviceCount++;
        }

        //5
        if (weight < 40 && age >= 10 && height > 100) {
            System.out.println("Zagrożenie anoreksją");
            adviceCount++;
        }

        //6
        if (weight > 100) {
            System.out.print("Dość ciężko, ");
            if (height > 200) {
                System.out.println("lecz przy tym wzroście waga jest uzasadniona");
                adviceCount++;
            } else {
                System.out.println("zastanów się, czy to nie za dużo?");
                adviceCount++;
            }
        }

        if (adviceCount > 0) {
            System.out.println("Liczba udzielonych porad: " + adviceCount);
        } else {
            System.out.printf("Niestety nie możemy nic doradzić - poczekaj na nowszą wersję oprogramowania");
        }
    }

    public static void main(String[] args) {

//        int age = 20;
//        int weight = 60; //kg
//        int height = 200; //cm

//        Napisz kod poradnika dietetycznego, analizującego następujące dane:
//        - wiek
//                - wzrost
//                - wagę
//
//        Na ich podstawie poradnik wyświetla na ekranie odpowiednie wskazówki:
//        1. nie udzielamy porad dla absurdalnych wartości
//        2. nie udzielamy porad ludziom starszym od 100 lat
//        3. porady dla nieletnich opatrzone specjalną klauzulą
//        4. ostre ostrzeżenie dla dziesięciolatków > 100kg
//        5. ostrzeżenie anorektyczne - dla osób > 10 lat, lżejszych od 40kg i większych od 1m
//        6. dla cięższych niż 100kg uwaga w zależności od wzrostu (dla większych niż 2m waga jest uzasadniona)
//        7. informacja o liczbie udzielonych porad
//
//        i co tam jeszcze wymyślicie ...


        giveAnAdvice(10, 100, 200);

        for (int age = 5; age < 100; age += 10) {
            for (int weight = 20; weight < 210; weight += 50) {
                for (int height = 80; height < 310; height += 30) {
                    giveAnAdvice(age, weight, height);
                }
            }
        }


    }
}
